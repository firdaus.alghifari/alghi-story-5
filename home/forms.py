from django import forms

class ActivityForm(forms.Form):
    start_time = forms.DateTimeField(input_formats = ['%Y-%m-%dT%H:%M'])
    end_time = forms.DateTimeField(input_formats = ['%Y-%m-%dT%H:%M'])
    message = forms.CharField(max_length = 100)
    place = forms.CharField(max_length = 100, required = False)
    category = forms.CharField(max_length = 100, required = False)
