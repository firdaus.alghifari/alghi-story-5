# Generated by Django 2.2.5 on 2019-10-06 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_auto_20191003_1124'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='category',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='activity',
            name='place',
            field=models.CharField(default='', max_length=100),
        ),
    ]
