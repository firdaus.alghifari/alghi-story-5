from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('contact/', views.contact, name='contact'),
    path('quote/', views.quote, name='quote'),
    path('schedule/', views.schedule, name='schedule'),
    path('addschedule/', views.addSchedule, name='addschedule'),
    path('removeschedule/', views.removeSchedule, name='removeschedule'),
]