from django.shortcuts import render, redirect
from .models import Activity
from .forms import ActivityForm

# Create your views here.

def index(request):
    return render(request, 'home/index.html')
    
def contact(request):
    return render(request, 'home/contact.html')
    
def quote(request):
    return render(request, 'home/quote.html')

def schedule(request):
    activity_list = Activity.objects.all()
    return render(request, 'home/schedule.html', {
        'activities': activity_list
    })

def addSchedule(request):
    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            start_time = request.POST['start_time']
            end_time = request.POST['end_time']
            message = request.POST['message']
            place = request.POST['place']
            category = request.POST['category']
            activity = Activity(start_time = start_time, end_time = end_time, message = message, place = place, category = category)
            activity.save()
    return redirect('/schedule/')

def removeSchedule(request):
    if request.method == 'POST' and 'id' in request.POST:
        activity = Activity.objects.get(id = request.POST['id'])
        activity.delete()
    return redirect('/schedule/')
