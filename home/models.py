from django.db import models

class Activity(models.Model):
    start_time = models.DateTimeField('Start activity')
    end_time = models.DateTimeField('End activity')
    message = models.CharField(max_length = 100)
    place = models.CharField(max_length = 100, default = "")
    category = models.CharField(max_length = 100, default = "")
